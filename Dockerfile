FROM python:3.6
ENV PYTHONBUFFERED=1
WORKDIR /usr/src/Task
COPY ./requirements.txt /usr/src/requirements.txt
RUN pip install -r /usr/src/requirements.txt
COPY . /usr/src/Task