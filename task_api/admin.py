from django.contrib import admin

from .models import Client, Mailing, Msg


# Register your models here.

@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'phone', 'operator_code', 'tag', 'TZ']
    list_editable = ['phone', 'operator_code', 'tag', 'TZ']


@admin.register(Mailing)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'date_start', 'date_end', 'msg', 'filter']
    list_editable = ['date_start', 'date_end', 'msg', 'filter']


@admin.register(Msg)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'date', 'status', 'mailing_id', 'client_id']
    list_editable = ['date', 'status', 'mailing_id', 'client_id']
