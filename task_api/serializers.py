from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from datetime import datetime
from .models import Client, Mailing


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MailingSerializer(ModelSerializer):
    id = serializers.CharField(max_length=20)

    class Meta:
        model = Mailing
        fields = ('id', 'date_start', 'date_end', 'msg', 'filter')
