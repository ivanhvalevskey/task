from django.urls import path, include
from . import views
from .views import ClientViewSet, MailingViewSet
from rest_framework import routers
from django.views.generic import RedirectView

client_router = routers.SimpleRouter()
client_router.register(r'client', ClientViewSet)

mailing_router = routers.SimpleRouter()
mailing_router.register(r'mailing', MailingViewSet)

urlpatterns = [
    path('', RedirectView.as_view(url='/swagger/'), name='docs'),
    path('api/v1/', include(client_router.urls)),
    path('api/v1/', include(mailing_router.urls)),
    path('api/v1/detail_mailing/<str:pk>/', views.DetailMailingAPIView, name='detail_mailing'),
    path('docs/', RedirectView.as_view(url='/swagger/'), name='docs'),
    path('mailing_create/', views.MailingCreate, name='mailing_create'),
    path('mailing_management/', views.MailingManagement, name='mailing_management'),
    path('mailing_admin_panel/', views.MailingAdminPanel, name='mailing_admin_panel'),
    path('mailing_msgs/<str:pk>/', views.MailingMsgs, name='mailing_msgs'),
    path('login/', views.Login, name='login'),
    path('logout/', views.Logout, name='logout'),
]



