from django.db import models
from datetime import datetime

from django.utils import timezone


class Client(models.Model):
    phone = models.CharField(max_length=11)
    operator_code = models.IntegerField()
    tag = models.CharField(max_length=50, blank=True, null=True, default=None)
    TZ = models.IntegerField(default=0)


class Msg(models.Model):
    date = models.DateTimeField(blank=True, null=True)
    status = models.BooleanField(default=False)
    mailing_id = models.CharField(max_length=20)
    client_id = models.IntegerField()


class Mailing(models.Model):
    id = models.CharField(primary_key=True, editable=False, max_length=20)
    date_start = models.DateTimeField(blank=True, null=True, default=timezone.now)
    date_end = models.DateTimeField(blank=True, null=True, default=None)
    time_start_point = models.TimeField(blank=True, null=True, default=datetime.strptime('0:0:0', '%H:%M:%S').time())
    time_end_point = models.TimeField(blank=True, null=True, default=datetime.strptime('0:0:0', '%H:%M:%S').time())
    msg = models.TextField(blank=True, null=True, default=None)
    filter = models.CharField(max_length=100, blank=True, null=True, default=None)
