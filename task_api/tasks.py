import json
from datetime import datetime, timedelta

import requests
from Task.celery import app
from celery.schedules import crontab
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Count
from rest_framework import status

from .funcs import nowutc_with_TZ
from .models import Client, Mailing, Msg


# {"tag": "green", "operator_code": "777"}


@app.task
def start_mailing(mailing_id: str, client_filter: str):
    print('--------------------------------------------------')
    mailing = Mailing.objects.get(id=mailing_id)

    # Filtration
    if client_filter != '':
        filters = dict(json.loads(client_filter))

        if filters.get('operator_code') and filters.get('tag'):
            clients = Client.objects.filter(operator_code=filters.get('operator_code'), tag=filters.get('tag'))
            print('Filter 1')
        elif filters.get('tag'):
            clients = Client.objects.filter(tag=filters.get('tag'))
            print('Filter 2')
        elif filters.get('operator_code'):
            clients = Client.objects.filter(operator_code=filters.get('operator_code'))
            print('Filter 3')
        else:
            print('Wrong filter. Mailing finished.')
            mailing.date_end = datetime.utcnow()
            return
    else:
        clients = Client.objects.all()

    print('Done filtration.')
    for client in clients.values():
        print(f'Client id {client["id"]}')
        print(f'Client TZ {client["TZ"]}')
        mailing = Mailing.objects.get(id=mailing_id)
        date_end = mailing.date_end
        msg_str = mailing.msg

        if date_end > datetime.utcnow():
            # create new Msg
            print('Create new Msg')
            msg = Msg.objects.create(status=0, mailing_id=mailing_id, client_id=client['id'])

            # data for the POST
            obj = {
                "id": client['id'],
                "phone": client['phone'],
                "text": msg_str
            }
            url = f'https://probe.fbrq.cloud/v1/send/{msg.id}'
            headers = {
                'Authorization': settings.API_TOKEN,
                'Content-Type': 'application/json',
            }

            # check interval
            print('Start check interval.')
            time_start_point = mailing.time_start_point
            time_end_point = mailing.time_end_point
            if time_start_point != datetime.strptime('0:0:0', '%H:%M:%S').time() \
                    and time_end_point != datetime.strptime('0:0:0', '%H:%M:%S').time()\
                    and time_start_point is not None and time_end_point is not None:
                if time_start_point < nowutc_with_TZ(client["TZ"]).time() < time_end_point:
                    print('In interval')
                    print(f'Client {client["id"]} trying to send msg.')
                    response = requests.post(url, headers=headers, json=obj)
                    msg.date = datetime.utcnow()
                    if not status.is_success(response.status_code):  # if error - try send later
                        msg.save()
                        send_one_msg.apply_async(kwargs={'msg_id': msg.id},
                                                 countdown=settings.TRY_SEND_MSG_AGAIN_IN_TIME)
                        print(f'Client {client["id"]} NOT sent msg. Response code: {response.status_code}. Try again in {settings.TRY_SEND_MSG_AGAIN_IN_TIME}')
                    else:
                        msg.status = 1
                        msg.save()
                        print(f'Client {client["id"]} sent msg. Response code: {response.status_code}')
                    print('--------------------------------------------------')
                    continue
                else:
                    print('Out interval')
                    # try later in in_time
                    today_with_start_point = datetime.combine(datetime.utcnow().date(), time_start_point)
                    if today_with_start_point < datetime.utcnow():
                        in_time = abs(int((today_with_start_point + timedelta(days=1) - nowutc_with_TZ(client["TZ"])).total_seconds()))+1
                    else:
                        in_time = abs(int((today_with_start_point - nowutc_with_TZ(client["TZ"])).total_seconds())) + 1
                    print(f'Send msg for client {client["id"]} later in {in_time} one msg')
                    send_one_msg.apply_async(kwargs={'msg_id': msg.id}, countdown=in_time)
                    print('--------------------------------------------------')
                    continue
            else:
                print('Interval is full.')
                # try to send
                response = requests.post(url, headers=headers, json=obj)

                # save Msg
                msg.date = datetime.utcnow()
                if not status.is_success(response.status_code):
                    print(f'Client {client["id"]} NOT sent msg. Response code: {response.status_code}.')
                    send_one_msg.apply_async(kwargs={'msg_id': msg.id}, countdown=settings.TRY_SEND_MSG_AGAIN_IN_TIME)
                    msg.save()
                else:
                    print(f'Client {client["id"]} sent msg. Responce code: {response.status_code}')
                    msg.status = 1
                    msg.save()
                print('--------------------------------------------------')

        else:
            print('--------------------------------------------------')
            return


@app.task
def send_one_msg(msg_id, attempt_number=0):
    print('Send one msg')
    msg = Msg.objects.get(id=msg_id)
    client = Client.objects.get(id=msg.client_id)
    mailing = Mailing.objects.get(id=msg.mailing_id)
    print(f'Client {client.id}')

    # if mailing ended
    if not mailing.date_start < datetime.utcnow() < mailing.date_end:
        return

    # data for the POST
    obj = {
        "id": client.id,
        "phone": client.phone,
        "text": mailing.msg
    }
    url = f'https://probe.fbrq.cloud/v1/send/{msg.id}'
    headers = {
        'Authorization': settings.API_TOKEN,
        'Content-Type': 'application/json',
    }

    # check interval
    print('Start check interval.')
    time_start_point = mailing.time_start_point
    time_end_point = mailing.time_end_point
    if time_start_point != datetime.strptime('0:0:0', '%H:%M:%S').time() \
            and time_end_point != datetime.strptime('0:0:0', '%H:%M:%S').time()\
            and time_start_point is not None and time_end_point is not None:
        print(f'time_start_point {time_start_point}')
        print(f'nowutc_with_TZ(client.TZ) {nowutc_with_TZ(client.TZ).time()}')
        print(f'time_end_point {time_end_point}')
        if time_start_point < nowutc_with_TZ(client.TZ).time() < time_end_point:
            print('In interval')
            print(f'Client {client.id} trying to send msg.')
            response = requests.post(url, headers=headers, json=obj)
            if not status.is_success(response.status_code):
                if attempt_number < 5:  # Count send attempt
                    send_one_msg.apply_async(kwargs={'msg_id': msg.id, 'attempt_number': attempt_number + 1},
                                             countdown=settings.TRY_SEND_MSG_AGAIN_IN_TIME)
                    print(f'Client {client.id} NOT sent msg. Response code: {response.status_code}. Try again in {settings.TRY_SEND_MSG_AGAIN_IN_TIME}')
                else:
                    print(f'5 раз подряд не удалось отправить сообщение на сервер')
            else:
                print(f'Client {client.id} sent msg. Response code: {response.status_code}')
                msg.status = 1
                msg.save()
        else:
            print('Out interval')
            today_with_start_point = datetime.combine(datetime.utcnow().date(), time_start_point + timedelta(hours=client.TZ))
            print(f'today_with_start_point {today_with_start_point}')
            if today_with_start_point < datetime.utcnow():
                in_time = abs(int((today_with_start_point + timedelta(days=1) - nowutc_with_TZ(client.TZ)).total_seconds())) + 1
            else:
                in_time = abs(int((today_with_start_point - nowutc_with_TZ(client.TZ)).total_seconds())) + 1
            print(f'Send msg for client {client.id} later in {in_time} one msg')
            send_one_msg.apply_async(kwargs={'msg_id': msg.id}, countdown=in_time)
    else:
        print('Interval is full.')
        response = requests.post(url, headers=headers, json=obj)
        if not status.is_success(response.status_code):
            if attempt_number < 5:  # Count send attempt
                send_one_msg.apply_async(kwargs={'msg_id': msg.id, 'attempt_number': attempt_number + 1},
                                         countdown=settings.TRY_SEND_MSG_AGAIN_IN_TIME)
                print(f'Client {client.id} NOT sent msg. Response code: {response.status_code}. Try again in {settings.TRY_SEND_MSG_AGAIN_IN_TIME}')
            else:
                print(f'5 раз подряд не удалось отправить сообщение на сервер')
        else:
            print(f'Client {client.id} sent msg. Response code: {response.status_code}')
            msg.status = 1
            msg.save()


@app.task
def send_daily_mailing_stats():
    stats_str = f'Statistics as of {datetime.utcnow().date()}\n'
    Mailings = Mailing.objects.filter(date_end__range=[f"{datetime.utcnow().date()}", f"{(datetime.utcnow() + timedelta(days=1)).date()}"])
    for mailing in Mailings:
        stats_str += get_mailing_stats_str(mailing.id)
    send_mail('Daily mailing stats', stats_str, settings.EMAIL_HOST_USER, [settings.EMAIL_FOR_SEND_STATS], fail_silently=False)


def get_mailing_stats_str(mailing_id: str):
    mailing = Mailing.objects.get(id=mailing_id)
    msgs = Msg.objects.filter(mailing_id=mailing_id)
    count_msg_grouped_by_status = msgs.values('status').annotate(total=Count('id'))

    mailing_stats = ''.join([
        f'Mailing {mailing.id}\n',
        f'    Start date: {mailing.date_start}\n',
        f'    End date: {mailing.date_end}\n',
        f'    Msg: {mailing.msg}\n',
        f'    Filter: {mailing.filter}\n',
        f'    Interval start point: {mailing.time_start_point}\n',
        f'    Interval end point: {mailing.time_end_point}\n',
        f'    Count msgs grouped by status: {list(count_msg_grouped_by_status)}\n\n'
    ])

    return mailing_stats


app.conf.beat_schedule = {
    'send_daily_mailing_stats-every-day': {
        'task': 'task_api.tasks.send_daily_mailing_stats',
        'schedule': crontab(minute=59, hour=23),
        # 'args': (16, 16),
    },
}
