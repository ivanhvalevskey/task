import json
from collections import OrderedDict
from datetime import datetime

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from rest_framework import mixins, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.viewsets import GenericViewSet

from .funcs import GenerateMailingId
from .models import Client, Mailing, Msg
from .serializers import ClientSerializer, MailingSerializer
from .tasks import start_mailing


def MailingAdminPanel(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            return render(request, 'task_api/mailing_admin_panel.html')

    return render(request, 'login.html', context={
        'invalid': 'You are not logged in as an administrator.'
    })

def MailingMsgs(request,pk):
    if request.user.is_authenticated:
        if request.user.is_staff:
            msgs = Msg.objects.filter(mailing_id=pk)
            return render(request, 'task_api/mailing_msgs.html', context={
                'msgs': msgs
            })

    return render(request, 'login.html', context={
        'invalid': 'You are not logged in as an administrator.'
    })
def MailingManagement(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            if request.user.is_staff:
                mailing = Mailing.objects.get(id=request.POST['id'])
                mailing.date_end = request.POST['date_end']
                mailing.time_start_point = request.POST['time_start_point']
                mailing.time_end_point = request.POST['time_end_point']
                mailing.msg = request.POST['msg']
                mailing.save()

    if request.user.is_authenticated:
        if request.user.is_staff:
            mailings = Mailing.objects.all().values()
            return render(request, 'task_api/mailing_management.html', context={
                'mailings': mailings
            })

    return render(request, 'login.html', context={
        'invalid': 'You are not logged in as an administrator.'
    })


def MailingCreate(request):
    if request.user.is_authenticated:
        if request.user.is_staff:
            return render(request, 'task_api/mailing_create.html')

    return render(request, 'login.html', context={
        'invalid': 'You are not logged in as an administrator.'
    })


def Login(request):
    if request.user.is_authenticated:
        return redirect('mailing_admin_panel')

    if request.method == "POST":
        username_ = request.POST['username']

        user = authenticate(request, username=username_, password=request.POST['password'])
        if user is not None:
            login(request, user)
            return redirect('mailing_admin_panel')
        else:
            return render(request, 'login.html', context={'invalid': 'Invalid username or password'})

    return render(request, 'login.html')


def Logout(request):
    logout(request)
    return redirect('login')


class CreateMailingMixin:
    def create(self, request, *args, **kwargs):
        print(request.data)

        # Check filters
        if request.data['filter'] != '':
            try:
                dict(json.loads(request.data['filter']))
            except json.decoder.JSONDecodeError:
                return Response('Wrong filters field.', status=status.HTTP_400_BAD_REQUEST,
                                headers={'Content-Type': 'application/json'})

        # Generate Mailing Id
        data = OrderedDict()
        data.update(request.data)
        data['id'] = GenerateMailingId()

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        creating = self.perform_create(serializer, request.data)
        if creating != 1:  # ERROR
            return Response(creating, status=status.HTTP_400_BAD_REQUEST,
                            headers={'Content-Type': 'application/json'})
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer, data):
        date_start = datetime.strptime(data['date_start'], "%Y-%m-%dT%H:%M")
        date_end = datetime.strptime(data['date_end'], "%Y-%m-%dT%H:%M")
        if date_start > date_end or date_end < datetime.utcnow():
            return 'The end date is less than the start date or the date now'
        in_time = int((date_start - datetime.utcnow()).total_seconds())
        serializer.save()
        print(serializer.data)
        if in_time < 0:
            print("Start now")
            start_mailing.delay(serializer.data['id'], serializer.data['filter'])
        else:
            print(f"Start Later in {in_time} seconds")
            start_mailing.apply_async(
                kwargs={'mailing_id': serializer.data['id'], 'client_filter': serializer.data['filter']},
                countdown=in_time)
        return 1

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


class MailingViewSet(CreateMailingMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.DestroyModelMixin,
                     # mixins.ListModelMixin,
                     GenericViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


class ClientViewSet(mixins.CreateModelMixin,
                    # mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    # mixins.ListModelMixin,
                    GenericViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


@api_view(['GET'])
def DetailMailingAPIView(request, pk):
    # permission_classes = (IsAdminUser,)
    # throttle_classes = [UserRateThrottle, AnonRateThrottle]

    mailing = Mailing.objects.filter(id=pk)
    if not mailing.exists():
        return Response(f'Mailing with id={pk} does not exist.', status=status.HTTP_404_NOT_FOUND,
                        headers={'Content-Type': 'application/json'})

    MAILING_INFO = dict(list(mailing.values())[0])

    MAILING_INFO['msgs'] = []
    msgs = Msg.objects.filter(mailing_id=MAILING_INFO['id'])
    for msg in msgs.values():
        MAILING_INFO['msgs'].append(dict(msg))

    return Response(MAILING_INFO, status=status.HTTP_200_OK, headers={'Content-Type': 'application/json'})
