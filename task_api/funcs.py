from .models import Mailing
from datetime import datetime, timedelta
ALPHABETS = {
    'en': 'abcdefghijklmnopqrstuvwxyz',
    'ru': 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
}

def nowutc_with_TZ(TZ):
    return datetime.utcnow() + timedelta(hours=int(TZ))

def GenerateMailingId():
    MailingId = random_str(length=20, alphabet='0123456789')
    while Mailing.objects.filter(id=MailingId).exists():
        MailingId = random_str(length=20)
    return MailingId

def random_str(length: int = 10, alphabet: str = ALPHABETS['en'], repete: bool = True, upper: bool = True,
               digits: bool = True):  # ru #digits
    import random

    if alphabet == 'ru':
        alphabet = ALPHABETS['ru']
    if alphabet == 'en' or alphabet == 'eu':
        alphabet = ALPHABETS['en']

    if digits:
        alphabet += '0123456789'

    if repete:
        rand_str = ''.join(random.choice(alphabet) for i in range(length))
    else:
        try:
            rand_str = ''.join(random.sample(alphabet, length))
        except ValueError:
            return "The alphabet is less than the length of the string. Generation without repetition is impossible."
    if upper:
        return rand_str.upper()
    else:
        return rand_str
