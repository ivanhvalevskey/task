# MailingAPI

## **Project deployment**
<br>

#### **With docker-compose**
1. Install `Docker`.
2. In the `terminal` from the folder with the file `docker-compose.yml`.

    ```
    $ docker-compose build
    $ docker-compose up -f
    $ docker exec -it CONTAINER(task_django) python manage.py createsuperuser
    $ docker-compose run django
    ```
3. Go to http://localhost:4444/ or http://localhost:4444/docs/ to view the API documentation.

Mailing management http://localhost:4444/mailing_admin_panel/
<br><br><br>
#### **Without docker-compose**
1. Project `Python` version `3.6`.
2. Cloning the repository.

    ```
    $ git clone https://gitlab.com/ivanhvalevskey/task.git
    ```
   
3. Open the project, install dependencies.

   ```
   python -m pip install -r requirements.txt
   ```
   or
   ```
   pip install -r requirements.txt
   ```
   
4. Run `Redis`, for example using `Docker'.
    ```
    docker run -d -p 6379:6379 redis
    ```
    To use other brokers, see
    [Broker Overview](https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/index.html#broker-overview)
    
    It is possible to change `port` and `host` for `Redis` in
    file `settings.py ` in the `REDIS_HOST` variables
    `REDIS_PORT`
    
5. In the folder with `manage.py ` create a `.env` file with the 
following contents and enter the appropriate data.
    ```
    SECRET_KEY=
    DATABASES_HOST=
    DATABASES_USER=
    DATABASES_PASS=
    DATABASES_NAME=
    API_TOKEN=
    EMAIL_HOST=
    EMAIL_HOST_USER=
    EMAIL_HOST_PASSWORD=
    EMAIL_PORT=
    EMAIL_FOR_SEND_STATS=
    ```
   
6. Perform migrations (in the terminal) while in the 
file folder `manage.py `.
    ```
    python manage.py makemigrations
    python manage.py migrate
    ```
7. In case of an error sending a message, you can
adjust the time after which
the next attempt to send via a variable will be made
`TRY_SEND_MSG_AGAIN_IN_TIME` в файле `settings.py`.

8. Replace in `settings.py`
    ```
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'postgres',
            'USER': 'postgres',
            'HOST': 'db_postgres',
            'PORT': 5432
        }
    }
    ```
   with
   ```
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': env('DATABASES_NAME'),
            'USER': env('DATABASES_USER'),
            'PASSWORD': env('DATABASES_PASS'),
            'HOST': env('DATABASES_HOST')
        }
    }
    ```

9. Starting the server `Django`.
    ```
    python manage.py runserver
    ```
10. Starting the server `Celery beat`.
    ```
    celery -A Task beat
    ```
11. Starting the server `Celery worker`.
    ```
    celery -A Task worker -l info --pool=solo
    ```
12. Go to the documentation tab and use the API.
    ```
    http://localhost/docs/
    ```

Mailing management http://localhost/mailing_admin_panel/
<br>
<br>

**`Были выполнены дополнительные задания:`**
```
3. Подготовить docker-compose для запуска всех сервисов проекта одной 
командой. 

С докером столкнулся впервые, поэтому, возможны ошибки.

5. Cделать так, чтобы по адресу /docs/ открывалась страница со 
Swagger UI.

6. Реализовать администраторский Web UI для управления рассылками 
и получения статистики по отправленным сообщениям.

8. Реализовать дополнительный сервис, который раз в сутки 
отправляет статистику по обработанным рассылкам на email.

9. Удаленный сервис может быть недоступен, долго отвечать на 
запросы или выдавать некорректные ответы. Необходимо 
организовать обработку ошибок и откладывание запросов при 
неуспехе для последующей повторной отправки. Задержки в 
работе внешнего сервиса никак не должны оказывать влияние 
на работу сервиса рассылок.

11. Реализовать дополнительную бизнес-логику: добавить в 
сущность "рассылка" поле "временной интервал", в котором 
можно задать промежуток времени, в котором клиентам можно 
отправлять сообщения с учётом их локального времени. Не 
отправлять клиенту сообщение, если его локальное время не 
входит в указанный интервал.
```