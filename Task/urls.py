from django.urls import path, include
from django.contrib import admin
from .yasg import urlpatterns as doc_urls

urlpatterns = [
    path('', include('task_api.urls')),
    path('admin/', admin.site.urls),
]
urlpatterns += doc_urls
