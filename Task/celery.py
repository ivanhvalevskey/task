import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Task.settings')

app = Celery('Task')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()



# app.conf.beat_schedule = {
#     'UUU-every-30-seconds': {
#         'task': 'task_api.tasks.UUU',
#         'schedule': crontab(minute=('*/1')),
#         'args': ('3')
#     },
# }
